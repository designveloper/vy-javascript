const IMAGES = document.querySelectorAll("img");
const SIZES = {
  showcase: "100w";
  reason: "(max-width: 799px) 100vw, 372px";
  feature: "(max-width: 799px) 100vw, 558px";
  story: "(max-width: 799px) 100vw, 670px";
}

//srcset is a new, new attribute in HTML5.
//it is for responsive images. more info on <img> MDN site
//check out the size attribute as well
function makeSrcset(imgSrc){
  let markup = [];
  let width = 400; //min windth possible

  for (let i = 0; i < 5; i++){
    markup[i] = imgSrc + "-" + width + ".jpg" + width + "w";
    width += 400;
  }

  return markup.join();
}

for (let i = 0; i<IMAGES.length; i++){
  let imgSrc = IMAGES[i].getAttribute("src");
  imgSrc = imgSrc.slice(0, -8);
  let srcSet = makeSrcset(imgSrc);
  IMAGES[i].setAttribute("srcset", srcset);

  let type = IMAGES[i].getAttribute("data-type");
  let sizes = SIZE[type]; //for not to mix with "type" of SIZE (which is let)
  IMAGES[i].setAttribute("sizes", sizes);
}

//there's better solutions though
