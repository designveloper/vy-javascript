# DSV - JavaScript

## Introduction
* **JS**: an interactive layer, runs in the browser and interacts w/ HTML markup and CSS rules to change what you see and what you can do.  
* [Babel transplier](https://babeljs.io "Translate ES6 to old JS")
* getMonth() will return the index month
* **How JS Loads**
	* Right away: HTML parsing > JS download > JS execution
    `<script src="script.js"></script>`
	* Asynchronous: JS download while HTML parsing, and kick in when JS is executed
    `<script src="script.js" async></script>`
	* Deferred: JS download while HTML parsing, but execute after everything has been loaded
		`<script src="script.js" defer></script>`
* **How to write JS**
	* JavaScript is case sensitive
	* use camelCase
		* variables start w/ lowercase letter
		* objs and classes start w/ uppercase letter
		* const are all-caps
	* whitespace only matters to humans (but still matter)
	* end each statement w/ a semicolon (for structure and readable purpose)
	* use comments liberally (and usually)

## Working with data
* #### Variables: The catch-all containers of JS
	* a(n) (empty) container, it holds whatever data we throw in
	* declaration: `var name;` (no space allowed)
	* to avoid gloabal scope, always declare your variables (with `var`)
* #### Data types in JS
	* **Numberic Data**
		* basically, number. Interger, negative, positive,..
	* **String Data**
		* stores strings of letters and symbols
		* what we commonly refer to as words and sentences
		* to declare one, use quotation mark, `""` or `''`
		* to use double quote inside string double quote, use `\"`
	* **Boolean Data**
		* `true` or `false`
	* **`null` Data**
		* intentional absence of a value, when you want a variable to be empty but not undefined
	* **`undefined` Data**
		* placeholder when a variable is not set
	* use `typeof` to find the type of data
* #### Arithmetic operators and math
	* assignment operator `=`
	* arithmetic operators: `+`, `-`, `*`, `/`
	* JS follows standard algebra rules
	* shorthand math: `+=`, `-=`, `*=`, `/=`
	* unary operator: `++`, `--`
		* prefix: add first, do command later
		* postfix: opposite of above
* #### Working with strings and numbers
	* add a string w/ number (`+`) will make the number be treatead as a string. The only exception operator.
	* with other operators (`-`, `/`,..) if the string contains *only* number, math will still happen. if not, the result will be NaN
* #### Conditional statement and logic
	* `if` statement
	* `===`: strict equality, identical in type and value
	* **XOR**: if a equals b XOR c equals d (not both) <br/>
	`((a == b || c == d) && ((a == b) != (c == d)))`
	* **Ternary operator**: `condition ? true : false`
* #### Arrays
	* how to create: declare an undefined variable, then add different value to it using square bracket and comma separated list. Or just assign a variable to an array.
	* [Array Reference](https://www.w3schools.com/jsref/jsref_obj_array.asp "w3school")
	* [MDN documentation for array](https://goo.gl/G4cTqk)

## Functions and Objects
* #### Three types of func
	* Named functions: executes when called by name
	* Anonymous functions: runs when triggered by specific events
	* Immediately invoked function expressions: run the moment browers encounter them
* #### ES6: let and const
	* `const`: constant
	* `let`: block scope variable, smaller scope than var (like in `if` block, loop block,...)
* #### Objects
  refer to code.01
* #### Dot and Bracket Notation
	* **Dot Notation**
		* `course.title`
		* may cause confusion to JS (i.e. if there's a property named "wp:image")
	* **Bracket Notation**
		* `course["title"]`
		* work well if we want to converse property name to string
	* **Closure**
      * Closure, a function inside a function that relies on variables in the outside function to work
      * [MDN article on closure](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures "example, detail, how to use,..")


## JS and DOM
* #### DOM - Document Object Model
	* The **Document Object Model** is a platform- and language-neutral interface that will allow programs and scripts to dynamically access and update the content, structure and style of documents. <br/>
	[w3c](https://www.w3.org/DOM/ "DOM defination")
	* [MDN reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
	* [Vietnamese reference](https://thachpham.com/web-development/javascript/dom-trong-javascript-can-ban.html "Căn bản về DOM trong JS")
* #### Target elements in the DOM
	* [Document interface reference](https://developer.mozilla.org/en-US/docs/Web/API/document)
	* `document.getElementById("some-ID")`
	* `document.getElementByClassName("className")`
	* `document.getElementByTagName("HTML tag")`
	* `document.querySelector(".main-nav a")`: get the first element matching specified selector(s) <br/>
	[MDN reference](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector "document.querySelector() reference")
	* `document.querySelectorAll(".post-content p")`: get all elements matching specified selector(s) <br/>
	[MDN reference](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll "document.querySelectorAll() reference") <br/>
	This return an array of elements that match the selector(s)
	* [Attribute selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors "MDN reference")
* #### Access and change elements
	* [Element](https://developer.mozilla.org/en-US/docs/Web/API/Element "MDN reference")
  * [HTML Element](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement "MDN reference")
	* with read-only element (eg classList), you must use its methods to interact with/change it
	* the same goes with attributes (refer to page element above for more info) <br/>
		* `element.hasAttribute`
		* `element.getAttribute`
		* `element.setAttribute`
		* `element.removeAttribute`
* #### Add DOM elements
  1. Create the element <br/>
    `.createElement()` - [reference](https://developer.mozilla.org/en-US/docs/Web/API/Document/createElement)
  2. Create the text node that goes inside the element <br/>
    `.createTextNode()` - [reference](https://developer.mozilla.org/en-US/docs/Web/API/Document/createTextNode)
  3. Add the text node to the element
  4. Add the element to the DOM tree <br/>
    `.appendChild()` - [reference](https://developer.mozilla.org/en-US/docs/Web/API/Node/appendChild) *(use in both step 3 and 4)* <br/>
    `.append()` - [reference](https://developer.mozilla.org/en-US/docs/Web/API/ParentNode/append) - will append any string to a specified apparent node *(use in place of step 2 and 3 - not support in older browser, use with care)*
* #### Apply inline CSS to an element
  * use `style` (reference [here](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement "style for HTML Element"))
  * use `.xxAttribute` methods
  * it's usually better (and safer) to create CSS rules and apply them to the elements
* #### DOM events
  whenever you interact with the browser in anyway, you fire an event
* #### Some typical DOM event
  * **Browser-level** event: browser behavior
    * `load`: when the res and dependents have finished loading
    * `error`: when a res failed to load
    * `online/offline`
    * `resize`
    * `scroll`
  * **DOM-level** event: content interaction
    * `focus` (clicked, tabbed to, touched, etc..)
    * `blur`: element loses focus
    * `resets/submit`: form-specific event
    * *Mouse events*: `click`, `mouseover`, `drap`, `drop`, etc..
  * **Other Events**
    * Media events
    * Progress events
    * CSS transition events
  * [Event Reference](https://developer.mozilla.org/en-US/docs/Web/Events "full list of avaiable event")
* #### Trigger functions with event handlers
  * ex: `.onclick`
  * work fine with DOM-level event and only trigger a func when event occurs
  * `.preventDefault()`
* #### Add and use event listeners
  * the event listener constantly listen to events that happen and respond to them
  * `element.addEventListener('event', func, false)`
  * [DOM event flow](https://wwww.w3.org/TR/DOM-Level-3-Events/#event-flow "more info on bubbling and capturing events")
* #### Pass arguments via event listeners
  * the event obj `e` auto pass on event trigger onto the function handle the event
  * use callback function to pass arg to func
  * ex: refer to code.02

## Loops
* [break](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/break)
* [continue](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/continue)

## Troubleshooting, Validating, and Minifying JS
* [Console Reference](https://developer.mozilla.org/en-US/docs/Web/API/Console)
  * `console.info`
  * `console.error`
* **Sources** tab in browser inspector. Literally a debugger.
* [JSLint](https://jslint.com "Online Script Linting") (it's *very* strict)
* [JSHint](https://jshint.com "Figure and sort out your problem") (it's nicer *less time-consuming*)
* Offline Debugger: ESLint
* [Minifying code](http://www.minifier.org/ "Online Minifying Tool")
  * use "Pretty Link" (top left corner) in source tab on browser to make the minified code readable again.
* Offline minifying: uglify-js-es

### Code Reference
#### Code.01
```javascript
var name = new Object();
var name = {
  property1: "property1",
  property2 : 2,
  method: function(){
    return ++property;
  }
}
```
#### Code.02
```javascript
function reveal(e, current){
    e.preventDefault();
    //do something here
}
element.addEventListener('click', function(e){ reveal(e, this);})
```
