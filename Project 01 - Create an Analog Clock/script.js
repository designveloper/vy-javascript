const HOURHAND = document.querySelector("#hour");
const MINUTEHAND = document.querySelector("#minute");
const SECONDHAND = document.querySelector("#second");
const MNSDEG = 6; /*minute and second position degree = 360/60*/

function runtheClock() {
  var date = new Date();
  let hr = date.getHours();
  let min = date.getMinutes();
  let sec = date.getSeconds();

  let hrPos = (hr*360/12) + (min*MNSDEG/12);
  let minPos = (min*MNSDEG) + (sec*MNSDEG/60);
  let secPos = sec*MNSDEG;

  HOURHAND.style.transform = "rotate(" + hrPos + "deg)";
  MINUTEHAND.style.transform = "rotate(" + minPos + "deg)";
  SECONDHAND.style.transform = "rotate(" + secPos + "deg)";
}

var interval = setInterval(runtheClock, 1000);
